
package ASA;

import java.util.ArrayList;

/**
 *
 * @author Hector Gonzalez Guerreiro
 * @since 15/04/2017
 * @version 4.0
 * 
 */
public abstract  class Expresion extends Bloque {
	protected Bloque Padre;
	protected Bloque Hijo;
	protected boolean isClausura = false;
	protected boolean isFinal = false;
	protected boolean isP = false;
	
	
	public Expresion (Bloque P){
		Padre = P;
	}
	
	
	
	@Override
	public boolean equals(Object Bl) {
		return Bl.equals(Hijo);
	}
	
	@Override
	public abstract String toString();

	@Override
	public Bloque getPadre() {
		return Padre;
	}

	@Override
	public Symbolo ObtenerSimbolo() {
		if(Hijo.ObtenerSimbolo() != null)
			return Hijo.ObtenerSimbolo();
		else
			return null;
	}

	@Override
	public abstract ArrayList<Bloque> consumir();

	@Override
	public  abstract ArrayList<Bloque> Puntear(boolean b);
	

	@Override
	public Bloque getHijos(int _index) {
		return Hijo;
	}

	
	@Override
	public abstract Bloque clonar(Bloque P);
	
	/**
	 * Permite a�adir una opcion a cualquier expresion expresion
	 * @param _Opc
	 */
	public void AddOpcion(Opcion _Opc) {
		Hijo = _Opc;	
	}

	/**
	 * Indica si La expresion es final y hay que insertarle un punto al final
	 */
	public void AnadirPunto() {
		isP = true;
	}
	/**
	 * Le asocia una concatenacion como su nuevo padre.
	 * @param _concatenacion Concatenacion Padre.
	 */
	public void setPadre(Concatenacion _concatenacion) {
		Padre=_concatenacion;
		
	}
	
	public boolean isFinal(){
		return isFinal;
	}

	

}
