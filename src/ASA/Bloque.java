/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ASA;

import java.util.ArrayList;

/**
 * Esta clase abstracta se encarga de generarar los diferentes tipos de nodos.
 *
 * @author Hector Gonzalez Guerreiro.
 * @since 19/04/2017
 * @version 4.0
 */
public abstract class Bloque {
    
    abstract public  boolean equals(Object Bl);
    abstract public String toString();
    /**
     * Permite la clonacion de cualquier Bloque dando un padre.
     * @param P Asociar la parte copiada un padre.
     * @return El bloque clonado,
     */
    abstract public Bloque clonar(Bloque P);
    /**
     * Obtienes el padre, es decir obtienes la estructura o bloque que te contiene.
     * @return Bloque clonado
     */
    abstract public Bloque getPadre();
    /**
     * Obtiene el simbolo con el punto delante de el.
     * @return Objeto symbolo, este elemeto contine el caracter que se va a consumir.
     */
    abstract public Symbolo ObtenerSimbolo();
    
    /**
     *Consumir un bloque implica dezplazar el punto a donde corresponda apartir de un bloque. 
     * @return La lista de posibles lugares del punto al dezplazarlo.
     */
    abstract public ArrayList<Bloque> consumir();
    
    /**
     * Inserto un punto en el bloque.
     * @param b Parametro boleano que indica cunado debe deterner.
     * 			Evita recursividad descontrolada.
     * @return  La listas de deribaciones de ese bloque. Es decir el conjunto de posibles localizaciones del punto.
     */
    abstract public ArrayList<Bloque> Puntear(boolean b);
    /**
     * Obtiene el hijo o en el caso mas concreto los elementos que contiene este bloque apartir de una posicion dada
     * @param _index Posicion del hijo que se desea obtener.
     * @return Devuelve el bloque 
     */
    abstract public Bloque getHijos(int _index);
    
	abstract public  boolean isFinal();
 
}
