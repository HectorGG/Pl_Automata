package ASA;

import java.util.ArrayList;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/

public class ExpresionPositiva extends Expresion{


	public ExpresionPositiva(Bloque P) {
		super(P);
	}

	@Override
	public String toString() {
		if(!isP)return "(" + Hijo + ")+";
		else return "(" + Hijo + ")+.";
	}

	@Override
	public Bloque clonar(Bloque P) {
		Expresion ex = new ExpresionPositiva(P);
		ex.Hijo = this.Hijo.clonar(ex);
		ex.isClausura = this.isClausura;
		ex.isFinal = this.isFinal;
		return ex;
	}

	
	@Override
	public ArrayList<Bloque> consumir() {
		ArrayList<Bloque> Lista_Bloques = new ArrayList<>();
		ArrayList<Bloque> Lista_Aux = new ArrayList<>();
		
		Lista_Bloques = Hijo.Puntear(false);
		
		if (Padre != null) {
			if(Padre.ObtenerSimbolo() != null) 
				Lista_Aux = Padre.ObtenerSimbolo().consumir();
			else 
				Lista_Aux = Padre.consumir();
			
		} else {
			Expresion nuevo = (ExpresionPositiva) this.clonar(null);
			nuevo.AnadirPunto();
			nuevo.isFinal = true;
			Lista_Bloques.add(nuevo);
		}
		
		for (int _Index_I = 0; _Index_I < Lista_Aux.size(); _Index_I++) {
			if (!Lista_Bloques.contains(Lista_Aux.get(_Index_I))) {
				Lista_Bloques.add(Lista_Aux.get(_Index_I));
			}
		}

		
		return Lista_Bloques;	
	}

	@Override
	public ArrayList<Bloque> Puntear(boolean b) {
		ArrayList<Bloque> Lista_Bloques = new ArrayList<>();

		if (!this.isFinal && !b) 
			Lista_Bloques = Hijo.Puntear(b);

		return Lista_Bloques;
	}

	

}
