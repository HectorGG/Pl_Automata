

import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;
import java.awt.FlowLayout;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JLabel;
import java.awt.Label;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class InterfazPrueba {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						InterfazPrueba window = new InterfazPrueba();
						window.frame.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Create the application.
	 */
	public InterfazPrueba() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 625, 171);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel IN = new JPanel();
		IN.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Entrada", TitledBorder.CENTER, TitledBorder.TOP, null, new Color(0, 0, 0)));
		IN.setBounds(0, 0, 484, 132);
		frame.getContentPane().add(IN);
		IN.setLayout(new BorderLayout(0, 0));
		
		JPanel Result = new JPanel();
		Result.setBorder(new TitledBorder(null, "Resultado", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		Result.setBounds(484, 0, 125, 132);
		frame.getContentPane().add(Result);
		Result.setLayout(null);
		
		JLabel rojo = new JLabel("");
		rojo.setBounds(10, 24, 32, 32);
		rojo.setIcon(new ImageIcon(this.getClass().getResource("Imagen/rojo.png")));
		Result.add(rojo);
		
		JLabel verde = new JLabel("");
		verde.setBounds(10, 67, 32, 32);
		verde.setIcon(new ImageIcon(this.getClass().getResource("Imagen/verde.png")));
		Result.add(verde);
		
		Label C = new Label("Correcto");
		C.setBounds(43, 24, 60, 32);
		Result.add(C);
		
		Label I = new Label("Incorrecto");
		I.setBounds(43, 67, 60, 32);
		Result.add(I);
		
		JTextArea Entrada = new JTextArea();
		Entrada.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				
				Automata c =new  Automata();
		           int state = 0;
		           int contador = 0;
		           do{
					   if(Entrada.getText().length() > 0 ){
							state = c.transition(state, Entrada.getText().charAt(contador));
							contador++;
					   }
					   
		           }while(contador < Entrada.getText().length());
		           
		           if(c.isFinal(state)){
		        	  C.setVisible(true);
		           	  rojo.setVisible(true);
		           	  I.setVisible(false);
		           	  verde.setVisible(false);
		           }
		           else{ 
		        	  C.setVisible(false);
		           	  rojo.setVisible(false);
		           	  I.setVisible(true);
		           	  verde.setVisible(true);
		           }
		    }
				
		});
		IN.add(Entrada, BorderLayout.CENTER);	
	}
}
