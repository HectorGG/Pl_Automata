/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFD;

import java.util.ArrayList;

import ASA.Expresion;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/
public class Automata {
    public ArrayList<Conjuntos> ListaConjuntos = new ArrayList<>();
    public String nombre;
    
    /**
     * Constructor Automata
     * @param raiz Arbol sintactico comleto.
     * @param nombre del automata.
     */
    public Automata(Expresion raiz,String nombre){
    	Conjuntos.setnum();
        Conjuntos _nuevo = new Conjuntos(raiz);
        ListaConjuntos.add(_nuevo);
        generar();
        this.nombre = nombre;
    }
    
    /**
     * Genera todos los conjuntos del automata.
     */
    private void generar(){
      for(int index = 0; index < ListaConjuntos.size(); index++){
    	  this.ListaConjuntos.get(index).GenerarConjuntos(ListaConjuntos);
      }
    }
    
    
    @Override
    public String toString(){
        String salida = "\n Nombre -> "+ nombre + "\n";
        for (int _Index = 0; _Index < ListaConjuntos.size(); _Index++) {
            salida = salida  +ListaConjuntos.get(_Index) +"\n";
        }
        return salida + "";
    }
    /**
     * @return Nombre del automata.
     */
	public String getNombre() {
		return this.nombre;
	}
	
	/**
	 * 
	 * @return Numero de conjuntos.
	 */
	public int getNumConjuntos() {
		return ListaConjuntos.size();
	}

	/**
	 * @param index Posicion del vector
	 * @return La lista de transiciones de ese conjunto.
	 */
	public ArrayList<Transiciones> getTrans(int index) {
		return ListaConjuntos.get(index).getTransiciones();
	}
	
	/**
	 * @param index Posicion del vector del conjunto.
	 * @return Conjunto 
	 */
	public Conjuntos getConjuntos(int index) {
		return ListaConjuntos.get(index);
	}
}
