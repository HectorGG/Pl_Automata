package Codigo;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import AFD.Automata;
import AFD.Transiciones;

public class GeneradorCODE {
	private Automata au;
	private String Transition;
	private String fin;
	
	public GeneradorCODE(Automata au){
		this.au = au;
		this.Transition = GenerarTransition();
		this.fin = GenerarFinal();
		this.excritura();
		
	}
	
	private String GenerarTransition(){
		String salida = "";
		salida = salida + " public class " + "Automata" + "{\n public Automata() {} \n";
		salida = salida + "\n public int transition(int state, char symbol) { \n "
				+ "switch(state) {\n";
		
		int numC = au.getNumConjuntos();
		for(int index = 0; index < numC; index++){
			ArrayList<Transiciones> ListaTransiciones = au.getTrans(index);
			salida = salida + "case " + index + ":\n"; 
			
			for(int trasnNum = 0; trasnNum < ListaTransiciones.size(); trasnNum++){
				salida = salida + "if(symbol ==" + ListaTransiciones.get(trasnNum).getSymbol().getCaracter() + ") return " + ListaTransiciones.get(trasnNum).getConjunto().id + ";\n";
			}
			
			salida = salida + "return -1;\n";
			
		}
		salida = salida + "default: \n  return -1;\n } \n }";
		return salida;
	}
	
	private String GenerarFinal(){
		String salida = "\n public boolean isFinal(int state){\n switch(state) { \n";
		int numC = this.au.getNumConjuntos();
		
		for(int index = 0; index < numC; index++){
			salida = salida + "case " + index + ": return";
			if(this.au.getConjuntos(index).esfinal()){
				salida = salida + " true;\n";
			}
			else{
				salida = salida + " false;\n";
			}
			
		}
		
		salida = salida + "default: return false;\n    } \n   } \n  }";
		
		return salida;
	}
	
	
	
	private void excritura(){
		 FileWriter fichero = null;
	     PrintWriter pw = null;
	    
	      try {
			fichero = new FileWriter("Ejecucion/Automata.java");
			pw = new PrintWriter(fichero);
			
			pw.print(Transition + fin);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
	           try {
	        	   
	           if (null != fichero)
	              fichero.close();
	           } catch (Exception e2) {
	              e2.printStackTrace();
	           }
	        }
	    
	}
	
	public String toString(){
		return Transition + "\n" + fin;
	}
}
