/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ASA;

import java.util.ArrayList;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/
public class Symbolo extends Bloque {

    private final Bloque Padre;
    private final String caracter;
    private boolean isPunto = false;

    public Symbolo(Bloque _P, String _caracter) {
        this.caracter = _caracter;
        this.Padre = _P;
        if(_caracter.equals("")) this.isPunto = true;
    }

    @Override
    public String toString() {
        if(isPunto) return "." + caracter ;
        else return caracter;
    }

    @Override
    public ArrayList<Bloque> Puntear(boolean b) {
        this.isPunto = true;
        Bloque _copiaR = this.raiz().clonar(null);
        this.isPunto = false;
        ArrayList<Bloque> Auxiliar = new ArrayList<>();
        Auxiliar.add(_copiaR);
        return Auxiliar;
      
    }

    @Override
    public Bloque clonar(Bloque P) {
       Symbolo sys  = new Symbolo(P, caracter);
       sys.isPunto = this.isPunto;
       return sys;
    }
    
    public Bloque raiz(){
        Bloque _Padre_R = this;
        while(_Padre_R.getPadre()!= null){
            _Padre_R = _Padre_R.getPadre();
        }
        return _Padre_R;
    }

    @Override
    public Bloque getPadre() {
        return Padre;
    }

    @Override
    public Symbolo ObtenerSimbolo() {
        if(this.isPunto)
            return this;
        else
            return null;
    }

    @Override
    public ArrayList<Bloque>consumir() {
        this.isPunto = false;
        return Padre.consumir();
    }

    @Override
    public boolean equals(Object Bl) {
       if(this.caracter.equals(((Symbolo)Bl).caracter) && this.isPunto ==  ((Symbolo)Bl).isPunto )
            return true;
        else
           return false;
    }

    @Override
    public Bloque getHijos(int _index) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    /**
     * Obtiene el caracter en String del simbolo.
     * @return Caracter.
     */
	public String getCaracter() {
		return this.caracter;
	}

	@Override
	public boolean isFinal() {
		return false;
	}


}
