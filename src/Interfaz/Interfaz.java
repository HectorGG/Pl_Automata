package Interfaz;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;

import Parse.AFDLexer;
import Parse.LexicalError;
import Parse.Token;
import Sintactico_Semantico.AFDParse;

import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.border.LineBorder;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import java.awt.TextArea;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;

public class Interfaz {

	private JFrame frmGeneradorDeAff;
	private String texto_Expresion = "";
	private boolean guardado = false;
	private JTextArea TLexico;
	private JEditorPane Expresion_R;
	private String rutaArchivo = "";
	private TextArea R_Automata;
	private AFDParse AFD = new AFDParse(this);

	/**
	 * Launch the application.
	 * 
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
			SwingUtilities.invokeLater(new Runnable() {
				public void run() {
					try {
						Interfaz window = new Interfaz();
						window.frmGeneradorDeAff.setVisible(true);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
	}

	/**
	 * Create the application.
	 */
	public Interfaz() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmGeneradorDeAff = new JFrame();
		frmGeneradorDeAff.setTitle("Generador de AFD");
		frmGeneradorDeAff.setResizable(false);
		frmGeneradorDeAff.setBounds(100, 100, 760, 629);
		frmGeneradorDeAff.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menu = new JMenuBar();
		frmGeneradorDeAff.setJMenuBar(menu);

		JButton btnAbrir = new JButton("Abrir");
		JButton BotonNuevo = new JButton("Nuevo");
		JButton GuardarNuevo = new JButton("GuardarComo");
		JButton btnGenerar = new JButton("Generar");
		JButton Guardar = new JButton("Guardar");
		
		menu.add(BotonNuevo);
		menu.add(btnAbrir);	
		menu.add(Guardar);
		menu.add(GuardarNuevo);
		menu.add(btnGenerar);
		
	    BotonNuevo.setIcon(new ImageIcon(getClass().getResource("/Imagen/Nuevo.png")));
		BotonNuevo.setHorizontalTextPosition(SwingConstants.CENTER);
		BotonNuevo.setVerticalTextPosition(SwingConstants.BOTTOM);
		
		btnAbrir.setIcon(new ImageIcon(getClass().getResource("/Imagen/Abrir.png")));
		btnAbrir.setHorizontalTextPosition(SwingConstants.CENTER);
		btnAbrir.setVerticalTextPosition(SwingConstants.BOTTOM);
		
		Guardar.setIcon(new ImageIcon(getClass().getResource("/Imagen/guardarComo.png")));
		Guardar.setHorizontalTextPosition(SwingConstants.CENTER);
		Guardar.setVerticalTextPosition(SwingConstants.BOTTOM);
		
		GuardarNuevo.setIcon(new ImageIcon(getClass().getResource("/Imagen/guardarComo.png")));
		GuardarNuevo.setHorizontalTextPosition(SwingConstants.CENTER);
		GuardarNuevo.setVerticalTextPosition(SwingConstants.BOTTOM);
		
		btnGenerar.setIcon(new ImageIcon(getClass().getResource("/Imagen/Compilar.png")));
		btnGenerar.setHorizontalTextPosition(SwingConstants.CENTER);
		btnGenerar.setVerticalTextPosition(SwingConstants.BOTTOM);
		
		
		
		
	    JButton btnEjePrueba = new JButton("Compilar");
		
		
		btnEjePrueba.setIcon(new ImageIcon(getClass().getResource("/Imagen/Compi.png")));
	    btnEjePrueba.setHorizontalTextPosition(SwingConstants.CENTER);
	    btnEjePrueba.setVerticalTextPosition(SwingConstants.BOTTOM);
	    menu.add(btnEjePrueba);
		
				btnEjePrueba.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						Ejecucion();
					}
				});
		
		
		frmGeneradorDeAff.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Expresion Regular", TitledBorder.CENTER, TitledBorder.TOP, null, null));
		panel.setBounds(10, 10, 728, 92);
		frmGeneradorDeAff.getContentPane().add(panel);
		panel.setLayout(new BorderLayout(0, 0));

		Expresion_R = new JEditorPane();
		panel.add(Expresion_R, BorderLayout.CENTER);

		JPanel R_Lexico = new JPanel();
		R_Lexico.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Lexico", TitledBorder.CENTER,
				TitledBorder.TOP, null, new Color(51, 51, 51)));
		R_Lexico.setBounds(10, 114, 262, 411);
		frmGeneradorDeAff.getContentPane().add(R_Lexico);
		R_Lexico.setLayout(new BorderLayout(0, 0));

		TLexico = new JTextArea();
		TLexico.setEditable(false);
		R_Lexico.add(TLexico, BorderLayout.CENTER);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(184, 207, 229)), "Analisis Automata",
				TitledBorder.CENTER, TitledBorder.TOP, null, new Color(51, 51, 51)));
		panel_1.setBounds(284, 115, 454, 410);
		frmGeneradorDeAff.getContentPane().add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		R_Automata = new TextArea();
		R_Automata.setEditable(false);
		panel_1.add(R_Automata, BorderLayout.CENTER);

		/**
		 * Apertura de un archivo. Para abrir un archivo debo comprobar si tengo
		 * que guardar el archivo.
		 * 
		 */
		btnAbrir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!texto_Expresion.equals(Expresion_R.getText()) && !guardado) {
					Object[] option = { "Guardar", "No Guardar", "Cancelar" };
					int n = JOptionPane.showOptionDialog(frmGeneradorDeAff, "Fichero sin guardar", "Texto sin guardar",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option, option[2]);

					if (n == 0) {
						guardarFicheroComo();
						Abri();
					}
					
					else if(n == 1){
						Abri();
					}					
					
				}
				else
					Abri();
				
			}
		});

		/**
		 * Generar.
		 */

		btnGenerar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (!texto_Expresion.equals(Expresion_R.getText()) && !guardado && !Expresion_R.getText().isEmpty()) {
					Object[] option = { "Guardar", "No Guardar", "Cancelar" };
					int n = JOptionPane.showOptionDialog(frmGeneradorDeAff, "Fichero sin guardar", "Texto sin guardar",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option, option[2]);
					
					if (n == 0) {
						TLexico.setText("");
						R_Automata.setText("");
						guardarFicheroComo();
						Generar();
					}

					else if (n == 1) {
						TLexico.setText("");
						R_Automata.setText("");
						rutaArchivo = System.getProperty("user.dir") + "\\textoAux.txt";
						guardarFichero();
						Generar();
					}
				}
				else{
					Generar();
				}
							
			}
		});

		/**
		 * Generar una nueva sesion de trabajo. Para ello debo comprobar si se
		 * ha escrito algo. Si ocurre debo decir al usuario si desea guardar.
		 */
		BotonNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!texto_Expresion.equals(Expresion_R.getText()) && !guardado && !Expresion_R.getText().isEmpty()) {
					Object[] option = { "Guardar", "No Guardar", "Cancelar" };
					int n = JOptionPane.showOptionDialog(frmGeneradorDeAff, "Fichero sin guardar", "Texto sin guardar",
							JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, option, option[2]);

					if (n == 0) {
						guardarFicheroComo();
						Expresion_R.setText("");
						TLexico.setText("");
						R_Automata.setText("");
					}

					else if (n == 1) {
						Expresion_R.setText("");
						TLexico.setText("");
						R_Automata.setText("");
						rutaArchivo = "";
					}

				}
			}
		});

		/**
		 * Guardado normal.
		 * 
		 */
		Guardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				guardarFichero();
				guardado = true;
			}
		});
		
		GuardarNuevo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				guardarFicheroComo();
				guardado = true;
			}
		});
		

		/**
		 * Ejecucion de la prueba.
		 * 
		 */
		
		/**
		 * Update de los caracteres.
		 */
		
		Expresion_R.addCaretListener(new CaretListener() {
			public void caretUpdate(CaretEvent arg0) {
				guardado = false;
			}
		});

	}

	public void EscribirError(String data) {
		this.TLexico.setCaretColor(Color.RED);
		this.TLexico.setText(data);
		this.TLexico.setCaretColor(Color.BLACK);
	}

	public void EscribirLexico(String data) {
		this.TLexico.setText(data);
	}

	public void EscribirAutomata(String string) {
		this.R_Automata.setText(string);
	}

	public void Ejecucion() {
		try {
			String salida = "cmd /c cd Ejecucion & javac -cp \".;./JTattoo-1.6.11.jar\" InterfazPrueba.java &  java  -cp \".;./JTattoo-1.6.11.jar\" InterfazPrueba";
			Runtime.getRuntime().exec(salida);
		} catch (IOException e) {
			System.out.println("Excepci�n: ");
			e.printStackTrace();
		}
	}

	public void Abri() {
		R_Automata.setText("");
		String aux = "", texto = "";
		JFileChooser filer = new JFileChooser();
		filer.showOpenDialog(frmGeneradorDeAff);

		File ficheroAbierto = filer.getSelectedFile();

		if (ficheroAbierto != null) {
			FileReader archivo;
			try {

				archivo = new FileReader(ficheroAbierto);
				BufferedReader lee = new BufferedReader(archivo);

				while ((aux = lee.readLine()) != null) {
					texto += aux + "\n";
				}

				texto_Expresion = texto;
				Expresion_R.setText(texto_Expresion);
				rutaArchivo = ficheroAbierto.getAbsolutePath();
				lee.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void Generar() {
		AFD = new AFDParse(this);
		String dataLexico = "";
		EscribirAutomata("");
		AFDLexer lexer;

		try {
			lexer = new AFDLexer(rutaArchivo);
			Token tk;
			do {
				tk = lexer.getNextToken();
				dataLexico = dataLexico + tk.toString() + "\n";
			} while (tk.getKind() != Token.EOF);

			EscribirLexico(dataLexico);
			AFD.parse(rutaArchivo);

		} catch (Exception | LexicalError e) {
			dataLexico = e.toString();
			EscribirError(dataLexico);
		}
	}

	public void guardarFichero() {
		FileWriter fichero = null;
		PrintWriter pw = null;

		try {

			fichero = new FileWriter(rutaArchivo);
			pw = new PrintWriter(fichero);
			System.out.println(Expresion_R.getText());
			pw.print(Expresion_R.getText());

		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			try {

				if (null != fichero)
					fichero.close();
			} catch (Exception e2) {
				e2.printStackTrace(); //
			}
		}
	}

	public void guardarFicheroComo() {
		JFileChooser jfiler = new JFileChooser();
		String ruta = "";

		try {
			if (jfiler.showSaveDialog(frmGeneradorDeAff) == JFileChooser.APPROVE_OPTION) {
				ruta = jfiler.getSelectedFile().getAbsolutePath();
				this.rutaArchivo = ruta;
				guardarFichero();
			}
		} catch (Exception e) {
			//
		}
	}
}