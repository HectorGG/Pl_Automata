/*
 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ASA;

import java.util.ArrayList;

/**
 * 
 *
 * @author Hector Gonzalez Guerreiro
 * @since 15/04/2017
 * @version 4.0
 * 
 */
public class Expresion_Normal extends Expresion {


	public Expresion_Normal(Bloque _P) {
		super(_P);
	}

	@Override
	public String toString() {
		if(!isP)return "(" + Hijo + ")";
		else return "(" + Hijo + ").";
	}



	@Override
	public ArrayList<Bloque> Puntear(boolean b) {
		ArrayList<Bloque> Lista_Bloques = new ArrayList<>();

		if (!this.isFinal && !b) 
			Lista_Bloques = Hijo.Puntear(b);

		return Lista_Bloques;
	}

	

	

	@Override
	public ArrayList<Bloque> consumir() {
		ArrayList<Bloque> Lista_Bloques = new ArrayList<>();
		ArrayList<Bloque> Lista_Aux = new ArrayList<>();

		if (Padre != null) {
			if(Padre.ObtenerSimbolo() != null) 
				Lista_Bloques = Padre.ObtenerSimbolo().consumir();
			else 
				Lista_Bloques = Padre.consumir();
				
		} else {
			Expresion nuevo = (Expresion) this.clonar(null);
			nuevo.AnadirPunto();
			nuevo.isFinal = true;
			Lista_Bloques.add(nuevo);
		}
		
		for (int _Index_I = 0; _Index_I < Lista_Aux.size(); _Index_I++) {
			if (!Lista_Bloques.contains(Lista_Aux.get(_Index_I))) 
				Lista_Bloques.add(Lista_Aux.get(_Index_I));
		}
		

		return Lista_Bloques;

	}
	
	@Override
	public Bloque clonar(Bloque P) {
		Expresion ex = new Expresion_Normal(P);
		ex.Hijo = this.Hijo.clonar(ex);
		ex.isClausura = this.isClausura;
		ex.isFinal = this.isFinal;
		return ex;
	}


	
}