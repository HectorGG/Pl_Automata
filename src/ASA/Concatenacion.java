/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ASA;

import java.util.ArrayList;

/**
 * Clase que concateana Expresiones O caracteres.
 * @author Hector Gonzalez Guerreiro
 * @since 19/04/2017
 * @version 4.0
 */
public class Concatenacion extends Bloque {

    private final Bloque Padre;
    private int PosPunto = 0;
    private final ArrayList<Bloque> Hijos = new ArrayList<>();

    /**
     * Al crear una concatenacion debo de pasarle el arbol del padre del que procede.
     * @param P Bloque Padre del arbol
     */
    public Concatenacion(Bloque P) {
        this.Padre = P;
    }

    @Override
    public String toString() {
        String _salida = "";
        for (int _Indice = 0; _Indice < Hijos.size(); _Indice++) {
            _salida = _salida + Hijos.get(_Indice);
        }
        return _salida;
    }
    
    /**
     * Inserta un simbolo en la concatenacion.
     * @param Sys El symbolo que deseo a�adir.
     */
    public void addSymbolo(Bloque Sys) {
        Hijos.add(Sys);
    }
    
    /**
     * Inserto una Expresion cualquiera en la concatenacion.
     * @param Ex Expresion que deseo insertar.
     */
    public void addBloque(Bloque Ex) {
        Hijos.add(Ex);
    }
    
    @Override
    public ArrayList<Bloque> Puntear(boolean b) {
        ArrayList<Bloque> ListaD = new ArrayList<>();
        PosPunto = 0;
        if (Hijos.size() > PosPunto) {
            ListaD.addAll(this.getHijos(PosPunto).Puntear(b));
        } else {
            ListaD = Padre.consumir();
        }

        return ListaD;
    }

    

    @Override
    public Bloque clonar(Bloque P) {
        Concatenacion _concatenacion = new Concatenacion(P);
        for (int _Indice = 0; _Indice < Hijos.size(); _Indice++) {
            _concatenacion.addBloque(Hijos.get(_Indice).clonar(_concatenacion));
        }
        _concatenacion.PosPunto = this.PosPunto;
        return _concatenacion;
    }

    @Override
    public Bloque getPadre() {
        return Padre;
    }

    @Override
    public Symbolo ObtenerSimbolo() {
        int _Index = 0;
        boolean encontrado = false;

        while (_Index < Hijos.size() && !encontrado) {
            if (Hijos.get(_Index).ObtenerSimbolo() != null) {
                encontrado = true;
            } else {
                _Index++;
            }
        }
        if (encontrado) {
            return Hijos.get(_Index).ObtenerSimbolo();
        } else {
            return null;
        }
    }

    @Override
    public ArrayList<Bloque> consumir() {
        ArrayList<Bloque> ListaD = new ArrayList<>();
        PosPunto++;
        if (Hijos.size() > PosPunto) {
            ListaD.addAll(this.getHijos(PosPunto).Puntear(false));
        } else {
            ListaD = Padre.consumir();
        }

        return ListaD;
    }

    @Override
    public boolean equals(Object Bl) {
        int _index = 0;
        boolean encontrado = true;

        while (_index < Hijos.size() && encontrado) {
            if (!Hijos.get(_index).equals(((Bloque) Bl).getHijos(_index))) {
                encontrado = false;
            } else {
                _index++;
            }
        }

        return encontrado;
    }

    @Override
    public Bloque getHijos(int _index) {
        return Hijos.get(_index);
    }

	@Override
	public boolean isFinal() {
		// TODO Auto-generated method stub
		return false;
	}
    

}
