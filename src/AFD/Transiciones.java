/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFD;

import ASA.Symbolo;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/
public class Transiciones {
    private Symbolo caracter;
    private Conjuntos destino;
    
    public Transiciones(Symbolo caracter){
        this.caracter = caracter;
    }
    
    /**
     * Inserta el conjunto destino.
     * @param C Conjunto de destino de la transicion.
     */
    public void insertarDestino(Conjuntos C){
        this.destino = C;
    }
    
    /**
     * Devuelve el caracter asociado a la transicion, es decir que symbolo consume para realizar la transicion.
     * @return
     */
    public Symbolo getSymbol(){
    	return caracter;
    }
    /**
     * Devuelve el conjunto destino.
     * @return Conjunto destino.
     */
    public Conjuntos getConjunto(){
    	return destino;
    }

    public String toString(){
        return caracter.toString().charAt(2) + "-> (" + destino.id + ")";
    }
}
