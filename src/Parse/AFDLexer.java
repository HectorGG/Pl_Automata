//------------------------------------------------------------------//
//                        COPYRIGHT NOTICE                          //
//------------------------------------------------------------------//
// Copyright (c) 2017, Francisco Jos� Moreno Velo                   //
// All rights reserved.                                             //
//                                                                  //
// Redistribution and use in source and binary forms, with or       //
// without modification, are permitted provided that the following  //
// conditions are met:                                              //
//                                                                  //
// * Redistributions of source code must retain the above copyright //
//   notice, this list of conditions and the following disclaimer.  // 
//                                                                  //
// * Redistributions in binary form must reproduce the above        // 
//   copyright notice, this list of conditions and the following    // 
//   disclaimer in the documentation and/or other materials         // 
//   provided with the distribution.                                //
//                                                                  //
// * Neither the name of the University of Huelva nor the names of  //
//   its contributors may be used to endorse or promote products    //
//   derived from this software without specific prior written      // 
//   permission.                                                    //
//                                                                  //
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND           // 
// CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,      // 
// INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF         // 
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE         // 
// DISCLAIMED. IN NO EVENT SHALL THE COPRIGHT OWNER OR CONTRIBUTORS //
// BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,         // 
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  //
// TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    //
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND   // 
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT          //
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING   //
// IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF   //
// THE POSSIBILITY OF SUCH DAMAGE.                                  //
//------------------------------------------------------------------//

//------------------------------------------------------------------//
//                      Universidad de Huelva                       //
//           Departamento de Tecnolog�as de la Informaci�n          //
//   �rea de Ciencias de la Computaci�n e Inteligencia Artificial   //
//------------------------------------------------------------------//
//                     PROCESADORES DE LENGUAJE                     //
//------------------------------------------------------------------//
//                                                                  //
//                  Compilador del lenguaje Tinto                   //
//                                                                  //
//------------------------------------------------------------------//

package Parse;

import java.io.*;

/**
 * Clase que desarrolla el analizador l�xico de Tinto
 * 
 * @author Francisco Jos� Moreno Velo
 *
 */
public class AFDLexer extends Lexer implements TokenConstants {

	/**
	 * Transiciones del automata del analizador l�xico
	 * 
	 * @param state Estado inicial
	 * @param symbol Simmbolo del alfabeto
	 * @return Estado final
	 */
	
	protected int transition(int state, char symbol) 
	{
		switch(state) 
		{
			case 0:
				if(symbol == '/') return 1;
				else if(symbol == ' ' || symbol == '\t' || symbol == '\r' || symbol == '\n' ) return 7;
				else if(symbol == '_') return 6;
				else if(symbol >= 'a' && symbol <= 'z') return 6;
				else if(symbol >= 'A' && symbol <= 'Z') return 6;
				else if(symbol == ':') return 9;
				else if(symbol == '|') return 12;	//F Reconoce OR
				else if(symbol == ';') return 13;	//F Reconoce SEMICOLON
				else if(symbol == '(') return 14;	//F Reconoce LPARENT
				else if(symbol == ')') return 15;   //F Reconoce RPARENT
				else if(symbol == '*') return 16; 	//F Reconoce START
				else if(symbol == '+') return 17; 	//F Reconoce PLUS
				else if(symbol == '?') return 18;   //F Reconoce HOOK
				else if(symbol == '\'') return 19;
			case 1:
				if(symbol == '*') return 2;
				else return -1;
			case 2:
				if(symbol == '*') return 3;
				else if(symbol != '*' && symbol != '/') return 2;
				else if(symbol == '/') return 2;
				else return -1;
			case 3:
				if(symbol == '*') return 4;
				else if(symbol == '/') return 5;	 //F Reconoce comentario multilinea//
				else if(symbol != '*' && symbol != '/') return 2;
				else return -1;
			case 4:
				if(symbol == '/') return 5;        //F Reconoce comentario multilinea//
				else if(symbol == '*') return 4;
				else return -1;
			case 6:
				if(symbol == '_') return 6;
				else if(symbol >= 'a' && symbol <= 'z') return 6;
				else if(symbol >= 'A' && symbol <= 'Z') return 6;
				else if(symbol >= '0' && symbol <= '9') return 6;		//F Reconoce ID //
				else return -1;
			case 7:
				if(symbol == ' ' || symbol == '\t' || symbol == '\r' || symbol == '\n') return 7; // F reconoce blanco
				else return -1;
			case 9:
				if(symbol == ':') return 10;
				else return -1;
			case 10:
				if(symbol == '=') return 11;							//F Reconoce EQ  //
				else return -1;
			case 19:
				if(symbol != '\'' && symbol !='\\' && symbol != '\n' && symbol !='\r') return 20;
				else if(symbol == '\\') return 21;
				else return -1;
			case 20:
				if(symbol == '\'') return 22;    // F Reconoce SYMBOL	//
				else return -1;
			case 21:
				if(symbol == 'n' || symbol == 't' || symbol == 'b' || symbol == 'r' || symbol == 'f' || symbol == '\\') return 20; 
				else return -1;
			default: return -1;
				
		}
	}

	/**
	 * Verifica si un estado es final
	 * 
	 * @param state
	 *            Estado
	 * @return true, si el estado es final
	 */
	protected boolean isFinal(int state) {
		if (state <= 0 || state > 23)
			return false;
		switch (state) {
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 9:
		case 10:
		case 19:
		case 20:
		case 21:
			return false;
		default:
			return true;
		}
	}

	/**
	 * Genera el componente l�xico correspondiente al estado final y al lexema
	 * encontrado. Devuelve null si la acci�n asociada al estado final es
	 * omitir (SKIP).
	 * 
	 * @param state
	 *            Estado final alcanzado
	 * @param lexeme
	 *            Lexema reconocido
	 * @param row
	 *            Fila de comienzo del lexema
	 * @param column
	 *            Columna de comienzo del lexema
	 * @return Componente l�xico correspondiente al estado final y al lexema
	 */
	protected Token getToken(int state, String lexeme, int row, int column) {
		switch (state) {
		case 4:
			return null;
		case 7:
			return null;
		case 6:
			return new Token(ID, lexeme, row, column);
		case 11:
			return new Token(EQ, lexeme, row, column);
		case 12:
			return new Token(OR, lexeme, row, column);
		case 13:
			return new Token(SEMICOLON, lexeme, row, column);
		case 14:
			return new Token(LPAREN, lexeme, row, column);
		case 15:
			return new Token(RPAREN, lexeme, row, column);
		case 16:
			return new Token(START, lexeme, row, column);
		case 17:
			return new Token(PLUS, lexeme, row, column);
		case 18:
			return new Token(HOOK, lexeme, row, column);
		case 22:
			return new Token(SYMBOL, lexeme, row, column);
		default:
			return null;

		}
	}

	/**
	 * Constructor
	 * 
	 * @param filename
	 *            Nombre del fichero fuente
	 * @throws IOException
	 *             En caso de problemas con el flujo de entrada
	 */
	public AFDLexer(String file) throws IOException {
		super(file);
	}

}
