package Sintactico_Semantico;


import Interfaz.Interfaz;
import java.io.IOException;

import AFD.Automata;
import ASA.Concatenacion;
import ASA.Expresion;
import ASA.ExpresionClausura;
import ASA.ExpresionCondicional;
import ASA.ExpresionPositiva;
import ASA.Expresion_Normal;
import ASA.Opcion;
import ASA.Symbolo;
import Codigo.GeneradorCODE;
import Parse.AFDLexer;
import Parse.LexicalError;
import Parse.Token;
import Parse.TokenConstants;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/

public class AFDParse implements TokenConstants {

    private AFDLexer Lexema;
    private Token nextToken;
    private String nombreAutomata;
    private Automata Au;
    private Interfaz Int;
    
    public AFDParse(Interfaz intz){
    	Int = intz;
    }
    
    
    /**
     * Genera el automata y el codigo si la cadena de caracteres correspode con el arbol semantico
     * @param Cadena de caracteres.
     * @return 
     */
    
    public boolean parse(String args) {
    	@SuppressWarnings("unused")
		String lexicoCODE = "";
        try {
        	
            this.Lexema = new AFDLexer(args);
            this.nextToken = Lexema.getNextToken();
            
            lexicoCODE += nextToken;
            
            Expresion Exp = parseCompilationUnit();
            
            if (nextToken.getKind() == EOF) {
            	Au = new Automata(Exp,this.nombreAutomata);
            	Int.EscribirAutomata(Au.toString());
            	@SuppressWarnings("unused")
				GeneradorCODE GC = new GeneradorCODE(Au);
                return true;
            } else {
                return false;
            }

        } catch (IOException | SintaxException  | LexicalError e) {
            Int.EscribirError(e.toString());
            return false;
        }
        

    }

    private Token match(int kind) throws SintaxException {
        Token salida = nextToken;
        if (nextToken.getKind() == kind) {
            nextToken = Lexema.getNextToken();
            return salida;
        } else {
            throw new SintaxException(nextToken, kind);
        }
    }

    /**
     * Crea una Expresion raiz.
     * @return La Expresion Raiz
     * @throws SintaxException
     * 
     */
    private Expresion parseCompilationUnit() throws SintaxException {
        int[] expected = {ID};
        Expresion ex = new Expresion_Normal(null);
        switch (nextToken.getKind()) {
            case ID:
            	nombreAutomata = nextToken.getLexeme();
                match(ID);
                match(EQ);
                Opcion opc = parseExpresion(ex);
                ex.AddOpcion(opc);
                match(SEMICOLON);
                return ex;
            default:
                throw new SintaxException(nextToken, expected);
        }
    }

    /**
     * Genera una opcion a partir de un padre expresion, esto nos sirve para asociar la nueva opcion un padre.
     * 'parseOption' y 'parsePrimaOpction' rellenara la estructura opcion y la devolvera apuntando a una expresion padre.
     * @param Padre Es el padre de Opcion que se va ha crear.
     * @return Una opcion ya construida.
     * @throws SintaxException
     */
    private Opcion parseExpresion(Expresion Padre) throws SintaxException {
        int[] expected = {SYMBOL, LPAREN};
        switch (nextToken.getKind()) {
            case SYMBOL:
            case LPAREN:
            	Opcion _Opc = new Opcion(Padre);
                parseOption(_Opc);
                parsePrimaOption(_Opc);
                return _Opc;

            default:
                throw new SintaxException(nextToken, expected);
        }
    }
    
    /**
     * Una opcion contiene tantas concatenaciones como or en la gramatica.
     * una concatenacion puede tener varios elementos pues parsePrimaBase sirbe para analizar si existe mas bases y a�adirlo a la concatenacion
     * @param opc Es la extructura que lo contiene es decir el padre en el caso de hablar de un arbol.
     * @throws SintaxException
     */
    private void parseOption(Opcion opc) throws SintaxException {
        int[] expected = {SYMBOL, LPAREN};
        Concatenacion _concatenacion = new Concatenacion(opc);
        switch (nextToken.getKind()) {
            case SYMBOL:
            case LPAREN:
               parseBase(_concatenacion);
               parsePrimaBase(_concatenacion);
               opc.addConcatenacion(_concatenacion);
               break;
            default:
                throw new SintaxException(nextToken, expected);
        }

    }
    
    /**
     * Si existe un or en la gramatica. Lo que hacemos es creae una nueva concatenacion que contendra mas elementos
     * @param _Opc Es la opcion que contendra varias concatenaciones.
     * @throws SintaxException
     */
    private void parsePrimaOption(Opcion _Opc) throws SintaxException {
        int[] expected = {RPAREN, OR, SEMICOLON};
        switch (nextToken.getKind()) {
            case OR:
                match(OR);
                parseOption(_Opc);
                parsePrimaOption(_Opc);
                break;
            case RPAREN:
            case SEMICOLON:
                break;
            default:
                throw new SintaxException(nextToken, expected);

        }

    }
    /**
     * En el parse BASE genero una Opcion. Esta Opcion contendra una lista de symbolo o expresiones.
     * Para ello le paso una opcion que se ira rellenando a medida que se llame al metodo.
     * @param Op 
     * @throws SintaxException 
     */
    private void parseBase(Concatenacion _concatenacion) throws SintaxException {
        int[] expected = {SYMBOL, LPAREN};
        switch (nextToken.getKind()) {
            case SYMBOL:
                Symbolo Sys = new Symbolo(_concatenacion,nextToken.getLexeme());
                match(SYMBOL);
                _concatenacion.addSymbolo(Sys);
                break;
            case LPAREN:
                match(LPAREN);
                Opcion Op = parseExpresion(null);
                match(RPAREN);
                Expresion Ex= parseOperacion(_concatenacion);
                Op.setPadre(Ex);
                Ex.AddOpcion(Op);
                _concatenacion.addBloque(Ex);
                break;

            default:
                throw new SintaxException(nextToken, expected);
        }

    }
    
    /**
     * Sigue analizando si mas bases-
     * @param _concatenacion si hay mas bases estos elementos se concatenan.
     * @throws SintaxException
     */
    private void parsePrimaBase(Concatenacion _concatenacion) throws SintaxException {
        int[] expected = {SYMBOL, LPAREN, OR, SEMICOLON};
        switch (nextToken.getKind()) {
            case SYMBOL:
            case LPAREN:
                parseBase(_concatenacion);
                parsePrimaBase(_concatenacion);
                break;
            case OR:
            case SEMICOLON:
            case RPAREN:
                break;
            default:
                throw new SintaxException(nextToken, expected);
        }

    }
    
    /**
     * Genera una Expresion de un tipo determinado, esto pueden ser normal,clausura,clausura positiva o de opcionalidad.
     * @param Padre Si procede de una expresion anterior este se le asocia una concatenacion.
     * @return La expresion.
     * @throws SintaxException
     */
    private Expresion parseOperacion(Concatenacion Padre) throws SintaxException {
        int[] expected = {SYMBOL, LPAREN, START, PLUS, HOOK, RPAREN, SEMICOLON};
        switch (nextToken.getKind()) {
            case SYMBOL:
            case LPAREN:
            case RPAREN:
            case OR:
            case SEMICOLON:
            	return  new Expresion_Normal(Padre);
            case START:
                match(START);
                return  new ExpresionClausura(Padre);
            case PLUS:
                match(PLUS);
                return  new ExpresionPositiva(Padre);
            case HOOK:
                match(HOOK);
                return  new ExpresionCondicional(Padre);
            default:
                throw new SintaxException(nextToken, expected);
        }

    }

    

}
