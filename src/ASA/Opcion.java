/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ASA;

import java.util.ArrayList;
/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/
public class Opcion extends Bloque {

    private Bloque Padre;
    private final ArrayList<Concatenacion> Hijos = new ArrayList<Concatenacion>();
    private int PosPunto = 0;

    public Opcion(Bloque _P) {
        this.Padre = _P;
    }

    @Override
    public String toString() {
        String _salida = "";
        for (int _indice = 0; _indice < Hijos.size(); _indice++) {
            if (_indice != Hijos.size() - 1) {
                _salida = _salida + Hijos.get(_indice) + "|";
            } else {
                _salida = _salida + Hijos.get(_indice);
            }
        }
        return _salida;
    }

    public void addConcatenacion(Concatenacion _concatenacion) {
        Hijos.add(_concatenacion);
    }

    @Override
    public ArrayList<Bloque> Puntear(boolean b) {
       ArrayList<Bloque> ListaD = new ArrayList<>();
       
        for (int ListaH = 0; ListaH < Hijos.size(); ListaH++) {
            ListaD.addAll(Hijos.get(ListaH).Puntear(b));
        }
        
        
        return ListaD;
    }
    

    @Override
    public Bloque clonar(Bloque P) {
        Opcion _Opc = new Opcion(P);
        for (int _Indice = 0; _Indice < Hijos.size(); _Indice++) {
            _Opc.Hijos.add((Concatenacion) Hijos.get(_Indice).clonar(_Opc));
        }
        _Opc.PosPunto = this.PosPunto;
        return _Opc;
    }

    @Override
    public Bloque getPadre() {
        return Padre;
    }

    

    @Override
    public Symbolo ObtenerSimbolo() {
        int _Index = 0;
        boolean encontrado = false;
        
        while(_Index < Hijos.size() && !encontrado){
            if(Hijos.get(_Index).ObtenerSimbolo() != null){
                encontrado = true;
            }
            else{
                _Index++;
            }
        }
        if(encontrado)
            return Hijos.get(_Index).ObtenerSimbolo();
        else 
            return null;
    }

    @Override
    public ArrayList<Bloque> consumir() {
        return this.Padre.consumir();
    }
    
    @Override
    public boolean equals(Object Bl) {
        int _index = 0;
        boolean encontrado = true;
        
        while(_index < Hijos.size() && encontrado){
            if(!Hijos.get(_index).equals(((Bloque)Bl).getHijos(_index)))
                encontrado = false;
            else
                _index++;
        }
        
        return encontrado;
    }
    
     @Override
    public Bloque getHijos(int _index) {
       return Hijos.get(_index);
    }
     
     /**
      * Permite sociar este bloque opcion a un padre Expresion
      * @param ex Expresion a la que se desea asociar.
      */
	public void setPadre(Expresion ex) {
		this.Padre = ex;
		
	}

	@Override
	public boolean isFinal() {
		return false;
	}

    

}
