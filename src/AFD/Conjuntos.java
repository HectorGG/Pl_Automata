/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AFD;

import java.util.ArrayList;

import ASA.Bloque;
import ASA.Expresion;
import ASA.Symbolo;

/**
*
* @author Hector Gonzalez Guerreiro
* @since 15/04/2017
* @version 4.0
* 
*/

public class Conjuntos {
	public ArrayList<Bloque> ListaDeribaciones = new ArrayList<>();
	private ArrayList<Transiciones> ListaTransiciones = new ArrayList<>();
	public int id;
	private boolean isFinal;
	public static int numConjuntos = 0;
	
	/**
	 * Genera las primeras deribaciones a partir de la estructura generada por el semantico.
	 * @param raiz Expresion que contiene toda la estructura.
	 */
	public Conjuntos(Expresion raiz) {
		ListaDeribaciones = raiz.Puntear(false);
		id = 0;
		numConjuntos++;
		determinarFinal();

	}
	/**
	 * Genera un nuevo conjunto a partir de las deribaciones.
	 * @param ls Conjunto de deribaciones que contiene ese conjunto.
	 */
	public Conjuntos(ArrayList<Bloque> ls) {
		id= numConjuntos;
		numConjuntos++;
		ListaDeribaciones = ls;
		determinarFinal();
	}
	
	/**
	 * Sirve para determinar si el conjunto es final.
	 */
	private void determinarFinal() {
		int _Index = 0;
		boolean esFinal = false;
		while(this.ListaDeribaciones.size() > _Index && !esFinal ){
			if(this.ListaDeribaciones.get(_Index).isFinal()){
				esFinal = true;
			}
			else 
				_Index++;
		}
		
		if(esFinal) this.isFinal = true;
		else this.isFinal = false;
		
	}
	/**
	 * 
	 * @return Indica si el conjunto es final de cadena.
	 */
	public boolean esfinal(){
		return this.isFinal;
	}
	
	
	/**
	 * Genera Nuevos conjuntos no repetidos.
	 * @param LExistentes Conjuntos existentem, se necesita para evitar repeticiones.
	 */
	public void GenerarConjuntos(ArrayList<Conjuntos> LExistentes){
		ArrayList<Bloque> Deribaciones;
		ArrayList<Bloque> Deribaciones_Nuevo_C = new ArrayList<>();
		Transiciones Tr;
		
		for(int index = 0; index < this.ListaDeribaciones.size();index++){
			Symbolo sys = ListaDeribaciones.get(index).ObtenerSimbolo();
			
			if(sys != null){
			Deribaciones = this.ObtenerDeribaciones(sys);
			
			for(int _deri_i = 0; _deri_i < Deribaciones.size(); _deri_i++){
				ArrayList<Bloque> Lista = Deribaciones.get(_deri_i).clonar(null).ObtenerSimbolo().consumir();
				
				for(int _deri_j = 0; _deri_j < Lista.size(); _deri_j++){
					if(!Deribaciones_Nuevo_C.contains(Lista.get(_deri_j))){
						Deribaciones_Nuevo_C.add(Lista.get(_deri_j));
					}
				}
				
				
			}
			
			int posicion = this.ExisteConjunto(LExistentes, Deribaciones_Nuevo_C);
			
			if(posicion == -1){
				Tr = new Transiciones(sys);
				Conjuntos Nuevo = new Conjuntos(extracted(Deribaciones_Nuevo_C));
				Tr.insertarDestino(Nuevo);
				LExistentes.add(Nuevo);
			}
			else{
				Tr = new Transiciones(sys);
				Tr.insertarDestino(LExistentes.get(posicion));
			}
			
			if(this.ExisteTransicion(sys) == -1)
				ListaTransiciones.add(Tr);
			
			Deribaciones_Nuevo_C.clear();
			
			}
		}
					
	}
	@SuppressWarnings("unchecked")
	private ArrayList<Bloque> extracted(ArrayList<Bloque> Deribaciones_Nuevo_C) {
		return (ArrayList<Bloque>) Deribaciones_Nuevo_C.clone();
	}
		
	/**
	 * Indica si existe una trasicion que consuma ese mismo symbolo en un conjunto.
	 * @param sys Symbolo que nos sirve para comprobar si existe esa transicion.
	 * @return
	 */
	
	private int ExisteTransicion(Symbolo sys) {
		int Index = 0;
		boolean encontrado = false;
		while(Index < ListaTransiciones.size() && !encontrado ){
			if(ListaTransiciones.get(Index).getSymbol().getCaracter().equals(sys.getCaracter())){
				encontrado = true;
			}
			else{
				Index++;
			}
		}
		if(encontrado)
			return Index;
		else
			return -1;
	}

	/**
	 * Comprueba que ese conjunto de bloque difiera completamante de algun conjunto existente.
	 * @param listaExistentes Lista de los conjuntos que ya existen.
	 * @param nuevasD Nuevas deribacions que se desea comprobar si hay repeticions.
	 * @return La posicion del conjunto en la tabla, si es -1 es que no existe.
	 * 
	 */
	private int ExisteConjunto(ArrayList<Conjuntos> listaExistentes, ArrayList<Bloque> nuevasD) {
		boolean existe = false;
		int index_C = 0;
		
		while(index_C < listaExistentes.size() && !existe){
			if( ExisteD(listaExistentes.get(index_C).ListaDeribaciones,nuevasD)){
				existe = true;
			}
			else{
				index_C++;
			}
		}
		
		if(!existe)
			index_C = -1;
		
		return index_C;
		
	}
	/**
	 * Indica si existe las mismas deribaciones que en un conjunto indicando si es el mismo.
	 * @param ListaD Lista de deribaciones del conjunto que ya existe.
	 * @param ListaNueva Lista de deribaciones nuevas que acaban de generarse.
	 * @return Indica si es igual o no.
	 */
	private boolean ExisteD(ArrayList<Bloque> ListaD, ArrayList<Bloque> ListaNueva) {
		boolean encontrado = false;
		
		if (ListaD.size() == ListaNueva.size()) {
			for(int index = 0; index < ListaNueva.size(); index++){
				if(ListaD.contains(ListaNueva.get(index))){
					encontrado = true;
				}
				else{
					return false;
				}
			}	
		}
		else 
			return false;
		return encontrado;
	}
	
	/**
	 * Obtienes el conjunto de deribaciones a partir del simbolo.
	 * @param sys Symbolo al que se desea deribar.
	 * @return Devuelve el conjunto de deribaciones a partir del symbolo.
	 */
	public ArrayList<Bloque> ObtenerDeribaciones(Symbolo sys){
		ArrayList<Bloque> ListaD = new ArrayList<>();
		for (int i = 0; i < ListaDeribaciones.size(); i++) {
			if(ListaDeribaciones.get(i).ObtenerSimbolo() != null && ListaDeribaciones.get(i).ObtenerSimbolo().getCaracter().equals(sys.getCaracter()))
				ListaD.add(ListaDeribaciones.get(i));
		}
		
		return ListaD;
		
		
	}

	@Override
	public String toString() {
		String salida =  "\t\t Conjunto: " + id + "\n\n";
		for (int _Index = 0; _Index < ListaDeribaciones.size(); _Index++) {
			salida = salida +"\t\t" + ListaDeribaciones.get(_Index) + "\n";

		}
		salida = salida + " \n\t\t [ ";
		for (int i = 0; i < ListaTransiciones.size(); i++) {
			salida = salida  + ListaTransiciones.get(i) + " , ";
		}
		return salida + "] \n";
	}
	/**
	 * 
	 * @return Lista de transiciones.
	 */
	public ArrayList<Transiciones> getTransiciones() {
		return ListaTransiciones;
	}
	
	public static void setnum(){
		numConjuntos = 0;
	}


}
